# Prelinker

This module allows preconnect domains and preload files to be added to either the response headers for HTTP/2 servers, or the as a `<Link>` in the `<Head>` section of the HTML.

Page restrictions can be applied, for example only loading a hero banner on the home page.